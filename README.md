# rt-task

[Тестовые_задания_для_соискателя.docx](task_3_taskfile%2F%D0%A2%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D1%8B%D0%B5_%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D1%8F_%D0%B4%D0%BB%D1%8F_%D1%81%D0%BE%D0%B8%D1%81%D0%BA%D0%B0%D1%82%D0%B5%D0%BB%D1%8F.docx)


## GET STARTED

task_1 and task_2:

* ``` python task_1/io_request_lib.py ```
* ``` python task_2/run.py ```

## Task_3

### Stack:
- [x] <a href="https://docs.sqlalchemy.org/en/20"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-plain.svg" alt="python" width="15" height="15"/> Python 3.10 <br/></a>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/fastapi/fastapi-plain.svg" alt="fastapi" width="15" height="15"/> Fastapi v.0.99.1 <br/>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-plain.svg" alt="docker" width="15" height="15"/> Docker Compose <br/>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-plain.svg" alt="postgresql" width="15" height="15"/> PostgreSQL 15.0 <br/>
- [x] <a href="https://docs.sqlalchemy.org/en/20"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sqlalchemy/sqlalchemy-plain.svg" alt="sqlalchemy" width="15" height="15"/> SqlAlchemy 2.0<br/></a>
- [x] <a href="https://docs.pydantic.dev/">🕳 Pydantic 1.10<br/></a>
- [x] <a href="https://alembic.sqlalchemy.org/en/latest/">⚗ Alembic 1.9.3<br/></a>


Copy `.env_example` as `.env`

* `$ docker compose up --build -d` - Create and run container


## Hosts:

* http://localhost:8089
* http://localhost:8089/api/v1/docs/ - документация

## Logs File

* PATH task_3/src/logs/app.log