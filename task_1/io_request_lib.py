import time

import asyncio
import aiohttp
import httpx


urls: list[str] = [
    "https://example.com", # https://www.youtube.com  https://www.bybit.com/fiat/trade/otc
    "https://e.mail.ru/",
    "https://github.com/",
    "https://beribit.com/exchange/spots/USDT_RUB",
    "https://www.bybit.com/fiat/trade/otc"
]*100

mock_headers = {"User-Agent": "PostmanRuntime/7.29.4"}


async def get_aiohttp_request(
        session: aiohttp.ClientSession,
        url: str
) -> None:
    try:
        async with session.get(url):
            pass
    except aiohttp.ServerTimeoutError:
        pass


async def net_runner_aiohttp() -> tuple[str, float]:
    start_time: float = time.time()
    tasks: list = []

    async with aiohttp.ClientSession(
            timeout=aiohttp.ClientTimeout(total=3, sock_read=1),
            headers=mock_headers
    ) as session:
        for url in urls:
            task = asyncio.create_task(get_aiohttp_request(session, url))
            tasks.append(task)
        await asyncio.gather(*tasks)
    return "Aiohttp", time.time() - start_time


async def net_runner_httpx() -> tuple[str, float]:
    start_time = time.time()

    async with httpx.AsyncClient(
            timeout=httpx.Timeout(timeout=3, read=1),
            headers=mock_headers
    ) as client:
        for url in urls:
            try:
                await client.get(url)
            except httpx.ReadTimeout:
                continue
    return "Httpx", time.time() - start_time


async def main():
    client_1, result_1 = await net_runner_httpx()
    print(f"Client {client_1} complete task at {result_1:.2f}")
    client_2, result_2 = await net_runner_aiohttp()
    print(f"Client {client_2} complete task at {result_2:.2f}")


if __name__ == "__main__":
    asyncio.run(main())

