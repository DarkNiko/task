
"""

Функция принимает в качестве аргумента набор ссылок.
Ссылки имеют формат ссылок на проекты на гитхабе 
(например: https://github.com/miguelgrinberg/Flask-SocketIO, https://github.com/miguelgrinberg/Flask-SocketIO.git).
Функция должна обработать полученные ссылки и вывести в консоль названия самих гит-проектов. 
Стоит рассмотреть защиту от ссылок "вне формата".

"""

import re


def extract_repo_names(urls: list) -> None:
    repo_pattern: re = r'https://github.com/[^/]+/(?P<repo_name>[^/.]+)(?<!\.git)$'

    for url in urls:
        match = re.search(repo_pattern, url)
        if match:
            print(match.group('repo_name'))
        else:
            print(f"Wrong format URL: {url}")


def main_a():
    urls: list[str] = [
        "https://github.com/miguelgrinberg/Flask-SocketIO",
        "https://github.com/miguelgrinberg/Flask-SocketIO.git",
        "https://example.com/wrong_format",
        "https://github.com/miguelgrinberg/Flask-sad/git"
    ]
    print(extract_repo_names(urls))
