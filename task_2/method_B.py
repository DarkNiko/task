
"""
Реализовать функцию, принимающую два списка и возвращающую словарь (ключ из первого списка, значение из второго),
упорядоченный по ключам. Результат вывести в консоль. Длина первого списка не должна быть равна длине второго.
Результат вывести в консоль.
"""

from typing import Any


def create_ordered_dict_optimized(
        keys: list,
        values: list
) -> dict[Any, Any]:
    result = dict(zip(keys, values))
    sort_dict = dict(sorted(result.items(), key=lambda item: item[1]))
    return sort_dict


def main_b():
    values = [5, 2, 1, 4]
    keys = ["Hoh", "Doh", "Moh"]
    print(create_ordered_dict_optimized(keys, values))
