
"""
Реализовать функцию с помощью методов map и lambda.
Функция принимает список элементов (состоящий из строк и цифр),
возвращает новый список, с условием - если элемент списка был строкой, в начало строки нужно добавить текст "abc_",
в конец строки - "_cba".
Если элемент был int - то его значение нужно возвести в квадрат. Результат вывести в консоль.
"""


def process_elements(elem: list) -> list:
    processed_elements = map(lambda x: ("abc_" + x + "_cba") if isinstance(x, str) else x ** 2, elem)
    return list(processed_elements)


def main_c():
    list_elem: list = ["haha", 15, "string"]
    print(process_elements(elem=list_elem))
