
"""
Реализовать функцию, которая замеряет время на исполнение 100 запросов к адресу: http://httpbin.org/delay/3.
Запросы должны выполняться асинхронно.
Допускается написание вспомогательных функций и использование сторонних библиотек.
Результат замера времени выводит в консоль. Ожидаемое время не должно превышать 10 секунд.
"""


import asyncio

import aiohttp
import time


async def fetch_url(session, url) -> int:
    async with session.get(url) as response:
        return response.status


async def net_runner_aiohttp() -> tuple[str, float]:
    start_time = time.time()
    url: str = "http://httpbin.org/delay/3"
    tasks: list = []

    async with aiohttp.ClientSession() as session:
        for _ in range(100):
            task = asyncio.create_task(fetch_url(session, url))
            tasks.append(task)
        await asyncio.gather(*tasks)
    return "Aiohttp", time.time() - start_time


def main_d():
    client, result = asyncio.run(net_runner_aiohttp())
    print(f"Client {client} complete task at {result:.2f}")

