"""
Написать класс, принимающий на вход текст.
Один метод класса должен выводить в консоль самое длинное слово в тексте.
Второй метод - самое часто встречающееся слово.
Третий метод выводит количество спецсимволов в тексте (точки, запятые и так далее).
Четвертый метод выводит все палиндромы через запятую.
"""
from task_2.method_F import timer_dec


class TextManager:

    def __init__(
            self,
            text: str
    ):
        self.text: str = text
        self.splited_text: list[str] = self.text.split()

    @timer_dec
    def longest_word(self) -> str:
        longest_word: str = max(self.splited_text, key=len)
        return longest_word

    @timer_dec
    def trand_word(self) -> str:
        cache: dict[str, int] = {}

        for word in self.splited_text:
            if word in cache:
                cache[word] += 1
            else:
                cache[word] = 1

        most_common = max(cache, key=cache.get)
        return most_common

    @timer_dec
    def special_char_count(self) -> int:
        special_chars = ',.;!?@#$%^&*()-=_+[]{}|\\:;"<>?/'
        count = sum(1 for char in self.text if char in special_chars)
        return count

    @timer_dec
    def palindromes(self) -> None:
        palindromes: list = []

        for word in self.splited_text:

            if len(word) > 2:
                clear_word: str = word.replace(".", "").replace(",", "")
                if clear_word.lower() == clear_word.lower()[::-1]:
                    palindromes.append(clear_word)

        print(", ".join(palindromes))


def main_e():
    ff = TextManager(text="Hello, my enane bester with proboblyios")
    print(ff.longest_word())
    print(ff.trand_word())
    print(ff.special_char_count())
    print(ff.palindromes())
