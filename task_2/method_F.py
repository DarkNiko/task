"""
Написать декоратор к предыдущему классу,
который будет выводить в консоль время выполнения каждого метода.
Результат выполнения задания должен быть оформлен в виде файла с кодом.
"""

import time


def timer_dec(func):
    def _wrapped_(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time() - start_time
        print(f"Время выполнения функции {func.__name__}: {end_time:.10f} секунд")
        return result
    return _wrapped_
