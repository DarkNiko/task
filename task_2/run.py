from task_2.method_A import main_a
from task_2.method_B import main_b
from task_2.method_C import main_c
from task_2.method_D import main_d
from task_2.method_E import main_e

if __name__ == "__main__":
    print(main_a())
    print('---'*40)
    print(main_b())
    print('---' * 40)
    print(main_c())
    print('---' * 40)
    print(main_d())
    print('---' * 40)
    print(main_e())


