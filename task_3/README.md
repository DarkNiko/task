# Carcas

---



### Stack:

- [x] <a href="https://docs.sqlalchemy.org/en/20"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-plain.svg" alt="python" width="15" height="15"/>
  Python 3.11 <br/></a>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/fastapi/fastapi-plain.svg" alt="fastapi" width="15" height="15"/> Fastapi v.0.110.1 <br/>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-plain.svg" alt="docker" width="15" height="15"/> Docker Compose <br/>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-plain.svg" alt="postgresql" width="15" height="15"/> PostgreSQL 15.0 <br/>
- [x] <a href="https://docs.sqlalchemy.org/en/20"><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sqlalchemy/sqlalchemy-plain.svg" alt="sqlalchemy" width="15" height="15"/>
  SqlAlchemy 2.0<br/></a>
- [x] <a href="https://docs.pydantic.dev/">🕳 Pydantic 1.10<br/></a>
- [x] <a href="https://alembic.sqlalchemy.org/en/latest/">⚗ Alembic 1.9.3<br/></a>
- [x] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/pytest/pytest-plain.svg" alt="redis" width="15" height="15"/> Pytest<br/>
- [ ] <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/redis/redis-plain.svg" alt="redis" width="15" height="15"/> Redis<br/>

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

### Run:

Copy `.env_example` as `.env`

### pre-commit hook

[Set up pre-commit](https://pre-commit.com)\
Each commit executes the hooks (automations) listed in
**. pre-commit-config.yaml**.
If it's not clear what error prevents you from committing, you can run the
hooks manually and see the errors:

* `pre-commit run --all-files`

### Commands to run the entire project in containers

* `$ docker compose up --build -d` - Create and run container
* `$ docker compose exec backend alembic upgrade head` - Make migrations
* `$ docker compose exec backend python main.py` - Starting the utility
* `$ docker compose exec backend pytest` - Running tests
* `$ docker compose down --rmi all` - Stop and delete docker containers
* `$ docker compose exec backend alembic upgrade head` - Make migrations
* `$ docker compose exec backend bash` - Enter container backend
* `$ docker compose logs backend` - Show logs

##### Access

* http://127.0.0.1:8001
* http://127.0.0.1:8001/api/v1/docs/ - документация
* http://127.0.0.1:8001/api/v1/admin/ - админка

local:

_Install requirements_

```
pip install fastapi[all]
pip install fastapi-users[sqlalchemy]
pip install alembic
pip install -r requirements.txt
```

_Initialization Alembic in directory "migrations"_

```
alembic init migrations
```

Migrations alembic:

```
alembic upgrade head
```

Reverse migration:

```
alembic downgrade -1
```init – prepares the project to work with alembic
upgrade – upgrade the database to a later version
downgrade – revert to a previous version
revision – creates a new revision file
```
