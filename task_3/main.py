
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.requests import Request
from fastapi.staticfiles import StaticFiles

from src.api.v1.api import apiv1
from src.config import (ALLOW_CREDENTIALS, BACKEND_CORS_ORIGINS, HEADERS,
                        METHODS, STATIC_DIR, TEMPLATES_DIR, settings)
from src.version import VERSION
from tasks.tasks import CheckResourceStatus, scheduler

PROJECT_NAME = settings.PROJECT_NAME
PROJECT_DESCRIPTION = settings.PROJECT_DESCRIPTION
ENVIRONMENT = settings.ENVIRONMENT


@asynccontextmanager
async def lifespan(app: FastAPI):
    scheduler.add_job(CheckResourceStatus.check_from_resource, 'interval', minutes=1)
    scheduler.start()
    print(f"{PROJECT_NAME} application started.")
    yield
    scheduler.remove_job(CheckResourceStatus.check_from_resource)
    scheduler.shutdown()
    print(f"{PROJECT_NAME} application finished.")


app = FastAPI(
    title=PROJECT_NAME,
    description=PROJECT_DESCRIPTION,
    version=VERSION,
    lifespan=lifespan,
)
app.mount("/admin/static", StaticFiles(directory=STATIC_DIR), name="static")
app.mount("/templates", StaticFiles(directory=TEMPLATES_DIR), name="templates")
app.mount("/static", StaticFiles(directory=STATIC_DIR), name="static")

app.add_middleware(
    CORSMiddleware,
    allow_origins=BACKEND_CORS_ORIGINS,
    allow_credentials=ALLOW_CREDENTIALS,
    allow_methods=METHODS,
    allow_headers=HEADERS,
)


@app.get("/healthcheck")
async def healthcheck():
    return True


app.mount("/api/v1", apiv1)


@app.middleware("http")
async def middleware_events(request: Request, call_next):
    """fix for sqladmin support"""
    if ENVIRONMENT != "local":
        request.scope["scheme"] = "https"
    response = await call_next(request)

    return response


@app.get("/")
def index():
    return f"{PROJECT_NAME} {ENVIRONMENT} v. {VERSION}"


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host="127.0.0.1",
        port=8001,
        reload=False,
    )
