from src.apps.auth.schemas import RefreshSessionCreate, RefreshSessionUpdate
from src.apps.user.models import RefreshSession
from src.core.bases.db_manager.dao import BaseDAO


class RefreshSessionDAO(BaseDAO[RefreshSession, RefreshSessionCreate, RefreshSessionUpdate]):
    model = RefreshSession
