from fastapi import HTTPException, status

TOKEN_EXPIRED: str = "Token expired"
INVALID_TOKEN: str = "Invalid token"
INVALID_CREDENTIALS: str = "Invalid username or password"
NOT_IMPLEMENTED: str = "Not implemented"


class NotImplemented(NotImplementedError):
    def __init__(self):
        pass


class InvalidCredentials(HTTPException):
    def __init__(self):
        detail: str = "Invalid username or password"
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=detail)


class InvalidToken(HTTPException):
    def __init__(self):
        detail: str = "Invalid token"
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=detail)


class TokenExpired(HTTPException):
    def __init__(self):
        detail: str = "Token has expired"
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=detail)


class UserAlreadyExists(HTTPException):
    def __init__(self):
        detail = "User already exists"
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail=detail)


class UserAlreadyAutenticated(HTTPException):
    def __init__(self):
        detail = "User already autenticated"
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail=detail)


class UserNotExists(HTTPException):
    def __init__(self):
        detail = "User not found"
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=detail)
