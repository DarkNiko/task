from typing import Annotated

from fastapi import Body, Depends, Request, Response
from fastapi.security import OAuth2PasswordRequestForm
from pydantic import EmailStr

from src.apps.auth import exceptions
from src.apps.auth.managers import AuthUserManager
from src.apps.auth.schemas import TokenResponse
from src.apps.user.dependencies import get_current_active_user
from src.apps.user.managers import UserManager
from src.apps.user.models import User
from src.apps.user.schemas import UserCreate, UserSchema


# TODO: Tests
async def login(
    response: Response, form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
) -> None:
    """Add tokens to cookies."""
    return await AuthUserManager.login(form_data, response)


async def login_jwt(
    response: Response, form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
) -> TokenResponse:
    """Returns JWT token without adding to cookies."""
    return await AuthUserManager.login_jwt(form_data, response)


async def logout(
    request: Request,
    response: Response,
    current_user: User = Depends(get_current_active_user),  # noqa
):
    result: dict = await AuthUserManager.logout(request, response)
    return result


async def logout_jwt(
    request: Request,
    response: Response,
    current_user: User = Depends(get_current_active_user),  # noqa
):
    result: dict = await AuthUserManager.logout_jwt(request, response)
    return result


async def register(
    request: Request,
    user_create: UserCreate,
) -> UserSchema:
    registered_user: User = await UserManager.register(request, user_create)
    return registered_user


# flake8: noqa: C901
async def request_verify_token(
    request: Request,
    email: EmailStr = Body(..., embed=True),  # noqa
    user: User = Depends(get_current_active_user),
):
    token: str = await UserManager.request_verify_token(user, request)

    # TODO: Success
    return None


# flake8: noqa: C901
async def verify(
    request: Request,
    token: str = Body(..., embed=True),
    user: User = Depends(get_current_active_user),  # noqa
):
    verified_user: User = await UserManager.verify(token, request)

    # TODO: Success
    return None


# TODO:
async def forgot_password(
    request: Request,
    email: EmailStr = Body(..., embed=True),
):
    try:
        user = await UserManager.get_by_email(email)
    except exceptions.UserNotExists:
        return None

    await UserManager.forgot_password(user, request)

    return None


# TODO:
async def reset_password(
    request: Request,
    token: str = Body(...),
    password: str = Body(...),
):
    await UserManager.reset_password(token, password, request)

    # TODO: Success
    return None
    # try:
    #     await user_manager.reset_password(token, password, request)
    # except (
    #     exceptions.InvalidResetPasswordToken,
    #     exceptions.UserNotExists,
    #     exceptions.UserInactive,
    # ):
    #     raise HTTPException(
    #         status_code=status.HTTP_400_BAD_REQUEST,
    #         detail=ErrorCode.RESET_PASSWORD_BAD_TOKEN,
    #     )
    # except exceptions.InvalidPasswordException as e:
    #     raise HTTPException(
    #         status_code=status.HTTP_400_BAD_REQUEST,
    #         detail={
    #             "code": ErrorCode.RESET_PASSWORD_INVALID_PASSWORD,
    #             "reason": e.reason,
    #         },
    #     )


# async def reset_password(user_data: ResetPassword, repository: Annotated[AuthRepository, Depends(get_auth_service)]):
# async def reset_password():
#     """Generate new random password."""
#     ...
#     # new_password: str = await self._db.reset_password(user_data.email)
#     #
#     # return new_password
#
#     raise exceptions.NotImplemented
#     # new_password: str = await repository.reset_password(user_data)
#     # # TODO here we must send this reset password to email and NOT returns new password in answer
#     #
#     # return BaseResponse.success(data={'new_password': new_password}, message=Messages.PASSWORD_RESET)

# async def reset_password(self, email: EmailStr) -> str:
#     """Generate and return new user password"""
#
#     user: User = await self.get_by_email(email)
#     if not user:
#         raise InvalidUserOrPassword
#
#     new_password: str = await get_random_string(NEW_PASSWORD_LENGTH)
#     logger.info(f'New password generated {new_password}')
#
#     await self.update_user_password(user.id, new_password)
#
#     return new_password
