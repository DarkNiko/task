import uuid
from datetime import datetime, timedelta, timezone

from fastapi.responses import Response

from src.apps.auth.dao import RefreshSessionDAO
from src.apps.auth.exceptions import (InvalidCredentials, InvalidToken,
                                      TokenExpired)
from src.apps.auth.schemas import (RefreshSessionCreate, RefreshSessionUpdate,
                                   TokenResponse)
from src.apps.auth.utils import generate_jwt, is_valid_password
from src.apps.user.dao import UserDAO
from src.apps.user.models import RefreshSession, User
from src.config import UserIDType, settings
from src.db.session import async_session_maker


class BaseAuthManager:
    secret = settings.SECRET_KEY
    algorithm = settings.JWT_ALGORITHM
    cookie_name = settings.COOKIE_NAME

    @classmethod
    async def _create_refresh_token(cls, user_id) -> UserIDType:
        refresh_token_expires = timedelta(days=settings.REFRESH_TOKEN_EXPIRE_DAYS)

        refresh_token = uuid.uuid4()
        async with async_session_maker() as session:
            await RefreshSessionDAO.create(
                session,
                RefreshSessionCreate(
                    user_id=user_id,
                    refresh_token=refresh_token,
                    expires_in=refresh_token_expires.total_seconds(),
                ),
            )
            await session.commit()

        return refresh_token

    @classmethod
    async def _set_session_cookies(cls, response, token_data):
        response.set_cookie(
            key=cls.cookie_name,
            value=token_data.access_token,
            max_age=settings.ACCESS_TOKEN_EXPIRE_MINUTES * 60,
            httponly=True,
        )

    @classmethod
    async def _delete_session_cookies(cls, response):
        response.delete_cookie(cls.cookie_name)

    @classmethod
    async def _authenticate(cls, username: str, password: str) -> User | None:
        async with async_session_maker() as session:
            user_exists = await UserDAO.find_one_or_none(session, username=username)
            if not user_exists:
                raise InvalidCredentials

            if user_exists and is_valid_password(password, user_exists.hashed_password):
                return user_exists

        # TODO:
        #  # Run the hasher to mitigate timing attack
        #  # Inspired from Django: https://code.djangoproject.com/ticket/20760
        #  self.password_helper.hash(credentials.password)
        return None

    @classmethod
    async def _reject_authentication(cls, token: uuid.UUID) -> None:
        async with async_session_maker() as session:
            refresh_session = await RefreshSessionDAO.find_one_or_none(
                session, RefreshSession.refresh_token == token
            )
            if refresh_session:
                await RefreshSessionDAO.delete(session, id=refresh_session.id)
            await session.commit()


class AuthUserManager(BaseAuthManager):

    @classmethod
    async def login(cls, form_data, response: Response):
        username = form_data.username
        password = form_data.password

        token_data, _ = await cls.login_user(password, username)
        if token_data:
            await AuthUserManager._set_session_cookies(response, token_data)

    @classmethod
    async def login_user(cls, password, username):
        user_exists = await cls._authenticate(username=username, password=password)
        if not user_exists:
            raise InvalidCredentials
        user_id = user_exists.id
        access_token = await generate_jwt(user_id)
        refresh_token = await cls._create_refresh_token(user_id)
        token_data: TokenResponse = TokenResponse(
            access_token=access_token, refresh_token=refresh_token, token_type="cookie"
        )
        if not token_data:
            raise InvalidCredentials

        return token_data, user_exists

    @classmethod
    async def login_jwt(cls, form_data, response):
        user_exists = await cls._authenticate(
            username=form_data.username, password=form_data.password
        )
        if not user_exists:
            raise InvalidCredentials

        user_id = user_exists.id
        encoded_jwt = await generate_jwt(user_id)
        access_token = f"Bearer {encoded_jwt}"
        refresh_token = await cls._create_refresh_token(user_id)
        token_data: TokenResponse = TokenResponse(
            access_token=access_token, refresh_token=refresh_token, token_type="bearer"
        )
        if not token_data:
            raise InvalidCredentials

        return token_data

    @classmethod
    async def logout(cls, request, response):
        await cls._delete_session_cookies(response)
        result: dict = {"message": "Logged out successfully"}
        return result

    @classmethod
    async def logout_jwt(cls, request, response=None):
        await AuthUserManager._reject_authentication(
            request.cookies.get("refresh_token")
        )
        result: dict = {"message": "Logged out successfully"}
        return result

    @classmethod
    async def refresh_token(cls, token: uuid.UUID) -> TokenResponse:
        async with async_session_maker() as session:
            refresh_session = await RefreshSessionDAO.find_one_or_none(
                session=session, filter=RefreshSession.refresh_token == token
            )

            if refresh_session is None:
                raise InvalidToken
            if datetime.now(timezone.utc) >= refresh_session.created_at + timedelta(
                seconds=refresh_session.expires_in
            ):
                await RefreshSessionDAO.delete(id=refresh_session.id)
                raise TokenExpired

            user = await UserDAO.find_one_or_none(session, id=refresh_session.user_id)
            if user is None:
                raise InvalidToken

            encoded_jwt = await generate_jwt(user.id)
            access_token = f"Bearer {encoded_jwt}"

            refresh_token_expires = timedelta(days=settings.REFRESH_TOKEN_EXPIRE_DAYS)

            refresh_token = uuid.uuid4()
            await RefreshSessionDAO.update(
                session,
                RefreshSession.id == refresh_session.id,
                obj_in=RefreshSessionUpdate(
                    refresh_token=refresh_token,
                    expires_in=refresh_token_expires.total_seconds(),
                ),
            )
            await session.commit()
            # return refresh_token
            # refresh_token = cls._update_refresh_token()

        return TokenResponse(
            access_token=access_token, refresh_token=refresh_token, token_type="bearer"
        )

    @classmethod
    async def abort_all_sessions(cls, user_id: UserIDType):
        async with async_session_maker() as session:
            await RefreshSessionDAO.delete(session, RefreshSession.user_id == user_id)
            await session.commit()
