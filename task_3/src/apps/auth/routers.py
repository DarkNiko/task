from fastapi import status
from fastapi.routing import APIRoute, APIRouter

from src.apps.auth import handlers, responses
from src.apps.auth.schemas import TokenResponse

auth_routes = [
    APIRoute(path="/login",
             endpoint=handlers.login,  # Auth:Cookie.Login
             methods=["POST"],
             response_model=None,
             status_code=status.HTTP_204_NO_CONTENT,
             summary='Check user credentials',
             description='Check user login and password. Returns JWT token and add it to cookies.',
             # responses=responses.login_responses,
             ),
    APIRoute(path="/logout",
             endpoint=handlers.logout,  # Auth:Cookie.Login
             methods=["POST"],
             # response_model=UserSchema,
             status_code=status.HTTP_200_OK,  # if not default
             summary='Logout',
             description=''
             ),
    APIRoute(path="/register",
             endpoint=handlers.register,  # Auth:Cookie.Login
             methods=["POST"],
             # response_model=UserSchema,
             status_code=status.HTTP_201_CREATED,  # if not default
             summary='Register new user',
             description='Register new user. Returns user info.',
             name="register:register",
             responses=responses.register_responses,
             ),
    APIRoute(path="/request-verify-token",
             endpoint=handlers.request_verify_token,  # Auth:Cookie.Login
             methods=["POST"],
             # response_model=UserSchema, # TODO: Success
             status_code=status.HTTP_202_ACCEPTED,  # if not default
             name='verify:request-token',
             # summary='Check user credentials',
             # description='',
             ),
    APIRoute(path="/verify",
             endpoint=handlers.verify,  # Auth:Cookie.Login
             methods=["POST"],
             # response_model=user_schema, # from schemas
             status_code=status.HTTP_202_ACCEPTED,  # if not default
             name="verify:verify",
             # summary='Check user credentials',
             # description='',
             responses=responses.verify_responses,
             ),
    APIRoute(path="/forgot-password",
             endpoint=handlers.forgot_password,  # Auth:Cookie.Login
             methods=["POST"],
             #     response_model=BaseResponse,
             status_code=status.HTTP_202_ACCEPTED,
             name="reset:forgot_password",
             # summary='',
             # description=''
             ),
    APIRoute(path="/reset-password",
             endpoint=handlers.reset_password,  # Auth:Cookie.Login
             methods=["POST"],
             #     response_model=BaseResponse,
             status_code=status.HTTP_200_OK,  # if not default
             summary='Reset user password',
             description='Generate new random password string, save it to user password and send to user email.'
             ),
]

auth_jwt_routes = [
    APIRoute(path="/login",
             endpoint=handlers.login_jwt,  # Auth:Cookie.Login
             methods=["POST"],
             response_model=TokenResponse,
             status_code=status.HTTP_200_OK,  # if not default
             summary='Check user credentials',
             description='Check user login and password. Returns JWT token and add it to cookies.',
             ),
    APIRoute(path="/logout",
             endpoint=handlers.logout_jwt,  # Auth:Cookie.Login
             methods=["POST"],
             # response_model=UserSchema,
             status_code=status.HTTP_200_OK,  # if not default
             summary='Logout',
             description=''
             ),
]

auth_router = APIRouter(prefix='/auth', tags=['Authorization'])
auth_router.include_router(APIRouter(routes=auth_routes))

auth_jwt_router = APIRouter(prefix='/auth/jwt', tags=['Authorization with JWT'])
auth_jwt_router.include_router(APIRouter(routes=auth_jwt_routes))
