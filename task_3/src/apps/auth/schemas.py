import uuid

from pydantic import BaseModel, Field

from src.config import UserIDType


class RefreshSessionCreate(BaseModel):
    refresh_token: uuid.UUID
    expires_in: int
    user_id: UserIDType


class RefreshSessionUpdate(RefreshSessionCreate):
    user_id: UserIDType | None = Field(None)


class TokenResponse(BaseModel):
    access_token: str
    refresh_token: UserIDType
    token_type: str
