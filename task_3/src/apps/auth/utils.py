from datetime import datetime, timedelta
from typing import Any, List

import jwt
from passlib.context import CryptContext
from pydantic import SecretStr

from src.config import UserIDType, settings

SecretType = str | SecretStr

pwd_context = CryptContext(
    schemes=["bcrypt"], deprecated="auto"
)  # TODO: Оставить только 1 hash


def is_valid_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


secret = settings.SECRET_KEY
algorithm = settings.JWT_ALGORITHM
cookie_name = settings.COOKIE_NAME


async def generate_jwt(user_id: UserIDType) -> str:
    """Returns token with encoded user id"""
    dt_now = datetime.utcnow()
    payload = {
        "exp": dt_now + timedelta(hours=settings.TOKEN_LIFETIME_HOURS),
        "iat": dt_now,
        "sub": str(user_id),
    }
    encoded_jwt = jwt.encode(payload, secret, algorithm=algorithm)
    return str(encoded_jwt)


async def generate_jwt_from_data(
    data: dict,
    secret: SecretType,
    lifetime_seconds: int | None = None,
    algorithm: str = settings.JWT_ALGORITHM,
) -> str:
    payload = data.copy()
    if lifetime_seconds:
        expire = datetime.utcnow() + timedelta(seconds=lifetime_seconds)
        payload["exp"] = expire
    return jwt.encode(payload, secret, algorithm=algorithm)


def decode_jwt(
    encoded_jwt: str,
    secret: SecretType,
    # audience: List[str],
    algorithms: List[str] = [settings.JWT_ALGORITHM],
) -> dict[str, Any]:
    # """Returns decoded from token user id"""

    try:
        payload = jwt.decode(
            jwt=encoded_jwt,
            key=secret,
            # audience=audience, # TODO: add audience as in fastapi-users
            algorithms=algorithms,
        )
    except Exception as e:
        print(f"{e=}")
    # except jwt.ExpiredSignatureError:
    #     raise exceptions.TokenExpired
    # except (jwt.InvalidTokenError, KeyError):
    #     raise exceptions.InvalidToken

    return payload
