from fastapi import Depends

from src.apps.log_stream.schemas import LogsInfoResponseSchema
from src.apps.log_stream.service import LogManager
from src.apps.user.dependencies import get_current_active_user
from src.apps.user.models import User


async def read_last_log_lines(current_user: User = Depends(get_current_active_user)) -> LogsInfoResponseSchema:
    lines: list[str] = await LogManager.get_last_lines_from_log()
    return LogsInfoResponseSchema(log_lines=lines)
