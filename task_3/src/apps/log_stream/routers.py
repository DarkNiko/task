from fastapi.routing import APIRoute, APIRouter
from starlette import status

from src.apps.log_stream import handlers

log_routes = [
    APIRoute(
        path="/read_last_log_lines",
        endpoint=handlers.read_last_log_lines,
        methods=["POST"],
        status_code=status.HTTP_200_OK,
        summary="Get my user data",
        description="Return current user data.(Authorized)",
    ),
]

log_router = APIRouter(prefix="/logs", tags=["Log Info"])
log_router.include_router(APIRouter(routes=log_routes))
