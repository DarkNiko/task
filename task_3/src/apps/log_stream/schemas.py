from pydantic import BaseModel


class BaseLogsModel(BaseModel):
    log_lines: list[str]


class LogsInfoResponseSchema(BaseLogsModel):
    ...
