from src.apps.user.models import User
from src.apps.user.schemas import UserCreateDB, UserUpdateDB
from src.core.bases.db_manager.dao import BaseDAO


class UserDAO(BaseDAO[User, UserCreateDB, UserUpdateDB]):
    model = User
