import uuid
from typing import Annotated

import fastapi
import jwt
from fastapi import Depends, HTTPException, status

from src.apps.auth.utils import decode_jwt
from src.apps.user import exceptions
from src.apps.user.managers import UserManager
from src.apps.user.models import User
from src.apps.user.utils import OAuth2PasswordWithCookie
from src.config import settings
from src.utils.logger_manageer import logger

oauth2_scheme = OAuth2PasswordWithCookie(tokenUrl="/api/v1/auth/login")


async def get_current_user(
        request: fastapi.Request,
        token: str = Depends(oauth2_scheme)
) -> User | None:
    try:
        payload = decode_jwt(
            encoded_jwt=token,
            secret=settings.SECRET_KEY,
            algorithms=[settings.JWT_ALGORITHM],
        )
        user_id = payload.get("sub")
        if user_id is None:
            logger.warn(f"-----------Invalid Token--------------\nClient data->: {request.client}")
            raise exceptions.InvalidToken
    except jwt.ExpiredSignatureError:
        raise exceptions.TokenExpired
    except (jwt.InvalidTokenError, KeyError):
        raise exceptions.InvalidToken
    except Exception:
        raise exceptions.InvalidToken

    current_user = await UserManager.get_user(uuid.UUID(user_id))
    if not current_user.is_verified:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="User is not verified"
        )

    return current_user


async def get_current_superuser(current_user: User = Depends(get_current_user)) -> User:
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Not enough privileges"
        )
    return current_user


async def get_current_active_user(
        request: fastapi.Request,
        current_user: User = Depends(get_current_user),
) -> User:
    if not current_user.is_active:
        logger.warn(f"-----------Not active User--------------\nClient data->: {request.client}")
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="User is not active"
        )
    return current_user


async def is_superuser(
        request: fastapi.Request,
        user: Annotated[User, Depends(get_current_user)],
) -> User:
    """Returns authenticated user instance if is superuser"""

    if user.is_superuser:
        return user
    logger.warn(f"-----------Not admin User--------------\n"
                f"Client data->: {request.client}\nUser data->:{user.username}")
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Access denied")
