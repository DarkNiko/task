from fastapi import HTTPException, status


class UserAlreadyExists(HTTPException):
    def __init__(self):
        detail = "User already exists"
        super().__init__(status_code=status.HTTP_409_CONFLICT, detail=detail)


class UserNotExists(HTTPException):
    def __init__(self):
        detail = "User not found"
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=detail)


class InvalidToken(HTTPException):
    def __init__(self):
        detail = "Invalid token"
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=detail)


class InvalidVerifyToken(HTTPException):
    def __init__(self):
        detail = "Invalid verify token"
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)


class InvalidResetPasswordToken(HTTPException):
    def __init__(self):
        detail = "Invalid reset token"
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)


class TokenExpired(HTTPException):
    def __init__(self):
        detail = "Token expired"
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=detail)


class UserInactive(HTTPException):
    def __init__(self):
        detail = "The user is inactive"
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)


class UserAlreadyVerified(HTTPException):
    def __init__(self):
        detail = "The user is already verified."
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)
