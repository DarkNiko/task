from fastapi import Depends

from src.apps.user import exceptions
from src.apps.user.dao import UserDAO
from src.apps.user.dependencies import (get_current_active_user,
                                        get_current_superuser)
from src.apps.user.managers import UserManager
from src.apps.user.models import User
from src.apps.user.schemas import UserSchema, UserUpdate
from src.config import UserIDType
from src.db.session import async_session_maker


async def get_current_user(
    current_user: User = Depends(get_current_active_user),
) -> UserSchema:
    if not current_user:
        raise exceptions.UserNotExists

    return UserSchema(**current_user.__dict__)


async def update_current_user(
    user_update: UserUpdate,
    current_user: User = Depends(get_current_active_user),  # noqa
) -> UserSchema:
    if not current_user:
        raise exceptions.UserNotExists

    return await UserManager.update_user(current_user, user_update)


async def get_user(
    user_id: UserIDType,
) -> UserSchema:
    target_user = await UserManager.get_user(user_id=user_id)
    return UserSchema(**target_user.__dict__)


async def update_user(
    user_id: UserIDType,
    user_update: UserUpdate,
    current_user: User = Depends(get_current_superuser),  # noqa
) -> UserSchema:
    async with async_session_maker() as session:
        db_user = await UserDAO.find_one_or_none(session, User.id == user_id)
        if db_user is None:
            raise exceptions.UserNotExists

        return await UserManager.update_user(db_user, user_update)
