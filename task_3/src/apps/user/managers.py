import jwt
from fastapi import Request
from pydantic import EmailStr, SecretStr

from src.apps.auth.helper import PasswordHelper
from src.apps.auth.utils import (decode_jwt, generate_jwt_from_data,
                                 get_password_hash)
from src.apps.user import exceptions
from src.apps.user.dao import UserDAO
from src.apps.user.models import User
from src.apps.user.schemas import (UserCreate, UserCreateDB, UserSchema,
                                   UserUpdate, UserUpdateDB)
from src.config import UserIDType, settings
from src.db.session import async_session_maker

SecretType = str | SecretStr

reset_password_token_audience: str = settings.RESET_PASSWORD_TOKEN_AUDIENCE
reset_password_token_secret: SecretType = settings.RESET_SECRET_KEY
reset_password_token_lifetime_seconds: int | None = (
    settings.RESET_TOKEN_LIFETIME_HOURS * 60 * 60
)


class UserManager:
    password_helper = PasswordHelper()
    a_session = async_session_maker

    @classmethod
    async def get_user(cls, user_id: UserIDType) -> User:
        async with cls.a_session() as session:
            db_user = await UserDAO.find_one_or_none(session, id=user_id)
            if db_user:
                return db_user
        raise exceptions.UserNotExists

    @classmethod
    async def get_by_email(
        cls,
        email: EmailStr,
    ) -> User:
        """Returns user instance by email"""

        async with cls.a_session() as session:
            db_user = await UserDAO.find_one_or_none(session, email=email)
            if db_user:
                return db_user
        raise exceptions.UserNotExists

    @classmethod
    async def update_user(cls, user: User, user_update: UserUpdate) -> UserSchema:

        user_id = user.id
        user_in = UserUpdateDB(
            **user_update.model_dump(
                exclude={"id", "is_active", "is_verified", "is_superuser"},
                exclude_unset=True,
            )
        )
        if user_update.password:
            user_in.hashed_password = get_password_hash(user_update.password)

        async with cls.a_session() as session:
            user_update = await UserDAO.update(
                session, User.id == user_id, obj_in=user_in
            )
            await session.commit()

            return UserSchema(**user_update.__dict__)

    @classmethod
    async def register(cls, request: Request, user_create: UserCreate) -> User:  # noqa
        async with cls.a_session() as session:
            user_exists = await UserDAO.find_one_or_none(
                session, email=user_create.email
            )
            if user_exists:
                raise exceptions.UserAlreadyExists

            user_create.is_superuser = False
            user_create.is_verified = True

            db_user: User = await UserDAO.create(
                session,
                UserCreateDB(
                    **user_create.model_dump(),
                    hashed_password=get_password_hash(user_create.password),
                ),
            )
            await session.commit()

        return db_user

    @classmethod
    async def on_after_request_verify(
        cls, user: User, token: str, request: Request | None = None
    ):
        """Here we can send email or phone verification"""
        print(f"Verification requested for user {user.id}. Verification token: {token}")
        #

    @classmethod
    async def on_after_verify(self, user: User, request: Request | None = None) -> None:
        """
        Perform logic after successful user verification.

        *You should overload this method to add your own logic.*

        :param user: The verified user.
        :param request: Optional FastAPI request that
        triggered the operation, defaults to None.
        """
        return  # pragma: no cover

    # flake8: noqa: C901
    @classmethod
    async def request_verify_token(
        cls, user: User, request: Request | None = None
    ) -> str:
        if not user.is_active:
            raise exceptions.UserInactive()
        if user.is_verified:
            raise exceptions.UserAlreadyVerified()

        verification_token_audience: str = settings.VERIFY_USER_TOKEN_AUDIENCE
        verification_token_secret: SecretType = settings.SECRET_KEY
        verification_token_lifetime_seconds: int | None = (
            settings.TOKEN_LIFETIME_HOURS * 60 * 60
        )

        token_data = {
            "sub": str(user.id),
            "email": user.email,
            # "aud": verification_token_audience,
        }

        token = await generate_jwt_from_data(
            data=token_data,
            secret=verification_token_secret,
            lifetime_seconds=verification_token_lifetime_seconds,
            algorithm=settings.JWT_ALGORITHM,
        )
        print(f"{token=}")

        await cls.on_after_request_verify(user, token, request)

        return token

    @classmethod
    async def verify(cls, token: str, request: Request | None = None):
        """
        Validate a verification request.

        Changes the is_verified flag of the user to True.

        Triggers the on_after_verify handler on success.

        :param token: The verification token generated by request_verify.
        :param request: Optional FastAPI request that
        triggered the operation, defaults to None.
        :raises InvalidVerifyToken: The token is invalid or expired.
        :raises UserAlreadyVerified: The user is already verified.
        :return: The verified user.
        """
        # try:
        data = decode_jwt(
            encoded_jwt=token,
            secret=settings.SECRET_KEY,
            # audience=[settings.VERIFY_USER_TOKEN_AUDIENCE],
            algorithms=[settings.JWT_ALGORITHM],
        )

        try:
            token_user_id = data["sub"]
            token_user_email = data["email"]
        except KeyError:
            raise exceptions.InvalidVerifyToken()

        try:
            target_verify_user_by_email = await cls.get_by_email(token_user_email)
        except exceptions.UserNotExists:
            raise exceptions.InvalidVerifyToken()

        if str(token_user_id) != str(target_verify_user_by_email.id):
            raise exceptions.InvalidVerifyToken()

        if target_verify_user_by_email.is_verified:
            raise exceptions.UserAlreadyVerified()

        print(f"Already: {target_verify_user_by_email.is_verified=}")

        async with cls.a_session() as session:

            user_dict: dict = target_verify_user_by_email.__dict__
            user_dict.update({"is_verified": True})
            user_in = UserUpdateDB(**user_dict)

            verified_user: User = await UserDAO.update(
                session, User.id == target_verify_user_by_email.id, obj_in=user_in
            )

            await session.commit()
            await cls.on_after_verify(verified_user, request)

        return verified_user

    @classmethod
    async def on_after_forgot_password(
        cls, user: User, token: str, request: Request | None = None  # noqa
    ):
        print(f"User {user.id} has forgot their password. Reset token: {token}")

    @classmethod
    async def forgot_password(cls, user: User, request: Request | None = None) -> None:
        """
        Start a forgot password request.

        Triggers the on_after_forgot_password handler on success.

        :param user: The user that forgot its password.
        :param request: Optional FastAPI request that
        triggered the operation, defaults to None.
        :raises UserInactive: The user is inactive.
        """
        if not user.is_active:
            raise exceptions.UserInactive()

        token_data = {
            "sub": str(user.id),
            "password_fgpt": cls.password_helper.hash(user.hashed_password),  # TODO
            # "aud": reset_password_token_audience,
        }
        print(f"{token_data=}")

        encoded_jwt = await generate_jwt_from_data(
            token_data,
            reset_password_token_secret,
            reset_password_token_lifetime_seconds,
        )
        print(f"{encoded_jwt=}")
        await cls.on_after_forgot_password(user, encoded_jwt, request)

    @classmethod
    async def on_after_reset_password(
        cls, user: User, request: Request | None = None
    ) -> None:
        """
        Perform logic after successful password reset.

        *You should overload this method to add your own logic.*

        :param user: The user that reset its password.
        :param request: Optional FastAPI request that
        triggered the operation, defaults to None.
        """
        return  # pragma: no cover

    @classmethod
    async def reset_password(
        cls, token: str, password: str, request: Request | None = None
    ):
        """
        Reset the password of a user.

        Triggers the on_after_reset_password handler on success.

        :param token: The token generated by forgot_password.
        :param password: The new password to set.
        :param request: Optional FastAPI request that
        triggered the operation, defaults to None.
        :raises InvalidResetPasswordToken: The token is invalid or expired.
        :raises UserInactive: The user is inactive.
        :raises InvalidPasswordException: The password is invalid.
        :return: The user with updated password.
        """
        try:
            data = decode_jwt(
                encoded_jwt=token,
                secret=reset_password_token_secret,
                # audience=[reset_password_token_audience],
            )
        except jwt.PyJWTError:
            raise exceptions.InvalidResetPasswordToken()

        print(f"{data=}")
        try:
            user_id = data["sub"]
            password_fingerprint = data["password_fgpt"]
        except KeyError:
            raise exceptions.InvalidResetPasswordToken()

        print(f"{user_id=} {password_fingerprint=} ")

        parsed_id = str(user_id)
        print(f"{parsed_id=}")

        targer_reset_user = await cls.get_user(user_id=parsed_id)
        print(f"{targer_reset_user=}")

        valid_password_fingerprint, _ = cls.password_helper.verify_and_update(
            targer_reset_user.hashed_password, password_fingerprint
        )
        print(f"{valid_password_fingerprint=}")

        if not valid_password_fingerprint:
            raise exceptions.InvalidResetPasswordToken()

        if not targer_reset_user.is_active:
            raise exceptions.UserInactive()
        print(f"{targer_reset_user.is_active=}")

        async with async_session_maker() as session:
            hashed_password = cls.password_helper.hash(password)
            user_dict: dict = targer_reset_user.__dict__
            user_dict.update({"hashed_password": hashed_password})
            user_in = UserUpdateDB(**user_dict)

            updated_user: User = await UserDAO.update(
                session, User.id == targer_reset_user.id, obj_in=user_in
            )

            await session.commit()
            print(f"{updated_user=}")

            await cls.on_after_reset_password(targer_reset_user, request)

        return updated_user
