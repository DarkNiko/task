import uuid
from datetime import datetime

import sqlalchemy as sa
from sqlalchemy import UUID, Boolean, String, func
from sqlalchemy.orm import Mapped, mapped_column

from src.config import UserIDType
from src.db.base import AbsBase, BaseUserDB


class User(BaseUserDB):
    __tablename__ = "users"

    username: Mapped[str] = mapped_column(String, unique=True, index=True)
    email: Mapped[str] = mapped_column(String, unique=True, index=True)
    hashed_password: Mapped[str] = mapped_column(String)
    is_active: Mapped[bool] = mapped_column(default=True)
    is_verified: Mapped[bool] = mapped_column(default=False)
    is_superuser: Mapped[bool] = mapped_column(Boolean, default=False)
    is_deleted: Mapped[bool] = mapped_column(Boolean, default=False)


class RefreshSession(AbsBase):
    __tablename__ = "refresh_session"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    refresh_token: Mapped[uuid.UUID] = mapped_column(UUID, index=True)
    expires_in: Mapped[int]
    created_at: Mapped[datetime] = mapped_column(
        sa.TIMESTAMP(timezone=True), server_default=func.now()
    )
    user_id: Mapped[UserIDType] = mapped_column(
        UUID, sa.ForeignKey(User.id, ondelete="CASCADE")
    )
