from fastapi import Depends, status
from fastapi.routing import APIRoute, APIRouter

from src.apps.user import handlers
from src.apps.user.dependencies import is_superuser
from src.apps.user.schemas import UserSchema

user_routes = [
    APIRoute(
        path="/me",
        endpoint=handlers.get_current_user,
        methods=["GET"],
        response_model=UserSchema,
        status_code=status.HTTP_200_OK,  # if not default
        summary="Get my user data",
        description="Return current user data.(Authorized)",
    ),
    APIRoute(
        path="/me",
        endpoint=handlers.update_current_user,
        methods=["PATCH"],
        response_model=UserSchema,
        status_code=status.HTTP_200_OK,  # if not default
        summary="Get my user data",
        description="Return current user data.(Authorized)",
    ),
    APIRoute(
        path="/{user_id}",
        endpoint=handlers.get_user,
        methods=["GET"],
        response_model=UserSchema,
        status_code=status.HTTP_200_OK,
        # dependencies=[Depends(is_superuser)],
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
    APIRoute(
        path="/{user_id}",
        endpoint=handlers.update_user,
        methods=["PATCH"],
        response_model=UserSchema,
        status_code=status.HTTP_200_OK,
        dependencies=[Depends(is_superuser)],
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
]
user_router = APIRouter(prefix="/users", tags=["User"])
user_router.include_router(APIRouter(routes=user_routes))
