from pydantic import BaseModel, Field

from src.config import UserIDType


class BaseUserScheme(BaseModel):
    username: str | None = Field(None)
    email: str | None = Field(None)
    is_active: bool = Field(True)
    is_verified: bool = Field(False)
    is_superuser: bool = Field(False)


class UserCreate(BaseUserScheme):
    password: str


class UserCreateDB(BaseUserScheme):
    hashed_password: str | None = None


class UserUpdate(BaseUserScheme):
    password: str | None = None


class UserUpdateDB(BaseUserScheme):
    hashed_password: str = None


class UserSchema(BaseUserScheme):
    id: UserIDType

    class Config:
        from_attributes = True
