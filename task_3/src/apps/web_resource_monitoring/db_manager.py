from src.apps.web_resource_monitoring.model import WebResourceModel
from src.apps.web_resource_monitoring.schemas import WebResourceCreateDBSchema, WebResourceUpdateDBSchema
from src.core.bases.db_manager.dao import BaseDAO


class WebResourceDAO(BaseDAO[WebResourceModel, WebResourceCreateDBSchema, WebResourceUpdateDBSchema]):
    model = WebResourceModel
