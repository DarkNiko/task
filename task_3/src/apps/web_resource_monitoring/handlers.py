import uuid

from fastapi import UploadFile, File, Depends

from src.apps.user import exceptions
from src.apps.user.dependencies import get_current_active_user
from src.apps.user.models import User
from src.apps.web_resource_monitoring.schemas import (
    WebResourceResponseSchema,
    WebResourceRequestCreateSchema, WebResourceResponseCreateFromFileSchema
)
from src.apps.web_resource_monitoring.service import WebResourceManager
from src.utils.logger_manageer import logger


async def create_by_url(
        data: WebResourceRequestCreateSchema,
        current_user: User = Depends(get_current_active_user)
) -> WebResourceResponseSchema:
    if current_user:
        manager = WebResourceManager()
        await manager.prepare_resource_data(url=data.url)
        return await manager.create()


async def create_by_csv(
        zip_file_urls: UploadFile = File(...),
        current_user: User = Depends(get_current_active_user)
) -> WebResourceResponseCreateFromFileSchema:
    if current_user:
        manager = WebResourceManager()
        try:
            response: dict = await manager.prepare_resource_from_file(urls_files=zip_file_urls)
            return WebResourceResponseCreateFromFileSchema(**response)
        except Exception as err:
            logger.exception(err)


async def get_all_resource(
        current_user: User = Depends(get_current_active_user)
) -> list[WebResourceResponseSchema]:
    if current_user:
        return await WebResourceManager.get_all()


async def get_by_uuid(
        resource_id: uuid.UUID,
        current_user: User = Depends(get_current_active_user)
) -> WebResourceResponseSchema:
    if not current_user:
        raise exceptions.UserNotExists
    return await WebResourceManager.get_by_uuid(obj_id=str(resource_id))
