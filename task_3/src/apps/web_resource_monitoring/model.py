
import uuid

from sqlalchemy import UUID, String, Integer, Boolean
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import Mapped, mapped_column

from src.db.base import BaseDB


class WebResourceModel(BaseDB):
    __tablename__ = "web_resource"

    id: Mapped[uuid.UUID] = mapped_column(
        UUID,
        primary_key=True,
        unique=True,
        index=True,
        nullable=False
    )
    url: Mapped[str] = mapped_column(
        String,
        unique=True,
        index=True
    )
    protocol: Mapped[str] = mapped_column(String)
    domain: Mapped[str] = mapped_column(String)
    sub_domain: Mapped[str] = mapped_column(String)
    path: Mapped[str] = mapped_column(String)
    params: Mapped[str] = mapped_column(
        JSONB,
        nullable=True
    )
    status_code: Mapped[int] = mapped_column(Integer)
    is_deleted: Mapped[bool] = mapped_column(
        Boolean,
        default=False
    )

    @classmethod
    async def a_to_dict(cls, obj) -> dict:
        result: dict = {key: value for key, value in obj.__dict__.items() if not key.startswith('_')}
        return result


