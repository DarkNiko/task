from fastapi.routing import APIRoute, APIRouter
from starlette import status

from src.apps.web_resource_monitoring import handlers
from src.apps.web_resource_monitoring.schemas import WebResourceResponseSchema, WebResourceResponseCreateFromFileSchema

resource_routes = [
    APIRoute(
        path="/create_by_url",
        endpoint=handlers.create_by_url,
        methods=["POST"],
        response_model=WebResourceResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get my user data",
        description="Return current user data.(Authorized)",
    ),
    APIRoute(
            path="/create_by_zip_csv",
            endpoint=handlers.create_by_csv,
            methods=["POST"],
            response_model=WebResourceResponseCreateFromFileSchema,
            status_code=status.HTTP_200_OK,
            summary="Get my user data",
            description="Return current user data.(Authorized)",
        ),
    APIRoute(
            path="/get_all_resources",
            endpoint=handlers.get_all_resource,
            methods=["GET"],
            status_code=status.HTTP_200_OK,
            response_model=list[WebResourceResponseSchema],
            summary="Get my user data",
            description="Return current user data.(Authorized)",
        ),
    APIRoute(
        path="/get_by_uuid/{resource_id}",
        endpoint=handlers.get_by_uuid,
        methods=["GET"],
        response_model=WebResourceResponseSchema,
        status_code=status.HTTP_200_OK,
        summary="Get user by id",
        description="Return user data by id.(Authorized)",
    ),
]
resource_router = APIRouter(prefix="/resource", tags=["Resource"])
resource_router.include_router(APIRouter(routes=resource_routes))
