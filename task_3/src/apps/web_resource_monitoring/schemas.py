import datetime
import uuid
from typing import Optional

from pydantic import BaseModel, Json


class BaseWebResourceSchema(BaseModel):
    url: str
    protocol: str
    domain: str
    sub_domain: str
    path: str
    params: Optional[Json]
    status_code: int
    is_deleted: Optional[bool] = False

    class Config:
        from_attributes = True


class WebResourceRequestCreateSchema(BaseModel):
    url: str


class WebResourceResponseCreateFromFileSchema(BaseModel):
    all_resource: int
    success_append: int
    fail_append: int


class WebResourceCreateDBSchema(BaseWebResourceSchema):
    pass


class WebResourceUpdateDBSchema(BaseModel):
    id: uuid.UUID
    url: Optional[str]
    protocol: Optional[str]
    domain: str
    sub_domain: str
    path: str
    params: Optional[Json]
    status_code: int
    is_deleted: Optional[bool] = False


class WebResourceResponseSchema(BaseWebResourceSchema):
    id: uuid.UUID
    is_deleted: bool
    created_at: datetime.datetime
    updated_at: datetime.datetime
    is_deleted: Optional[bool]

    class Config:
        from_attributes = True
