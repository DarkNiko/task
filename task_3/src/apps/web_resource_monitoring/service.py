import asyncio
import json
import urllib
import uuid
from typing import Any
from urllib.parse import urlparse

import httpx
from fastapi import HTTPException, UploadFile

from src.apps.web_resource_monitoring.db_manager import WebResourceDAO
from src.apps.web_resource_monitoring.schemas import WebResourceCreateDBSchema, WebResourceResponseSchema, \
    WebResourceUpdateDBSchema
from src.db.session import async_session_maker
from src.utils.file_manager import FileManager
from src.utils.logger_manageer import logger


class WebResourceParserFetcher:
    mock_headers: dict = {"User-Agent": "PostmanRuntime/7.29.4"}

    def __init__(self, url: str):
        self.url: str = url

    async def parse_detail_url(self) -> dict:
        result = urlparse(self.url)

        status_code = await self._get_status_code()

        if status_code:
            resource_dict: dict = await self._make_dict(
                id=uuid.uuid4(),
                url=self.url,
                protocol=result.scheme,
                domain=result.hostname.split('.')[0],
                sub_domain=result.hostname.split('.')[-1],
                path=result.path,
                params=await self._prepare_query_params(q_params=urllib.parse.parse_qs(result.query)),
                status_code=status_code
            )
            return resource_dict
        logger.exception(f"Bad format url: {self.url}")
        raise HTTPException(status_code=409, detail=f"Bad format url: {self.url}")

    @classmethod
    async def _make_dict(cls, **kwargs) -> dict:
        return kwargs

    async def _get_status_code(self) -> int | None:
        async with httpx.AsyncClient(
                timeout=httpx.Timeout(timeout=3, read=3),
                headers=self.mock_headers
        ) as client:
            try:
                result = await client.get(self.url)
                if result.status_code < 500:
                    return result.status_code
                raise HTTPException(
                    status_code=409,
                    detail=f"Response status from page {self.url} is {result.status_code}"
                )
            except (httpx.ConnectError, httpx.UnsupportedProtocol) as err:
                logger.exception(err)
                return None

    @classmethod
    async def _prepare_query_params(cls, q_params: dict[str, list]) -> str | None:
        if q_params:
            single_value_params_dict = {k: v[0] for k, v in q_params.items()}
            return json.dumps(single_value_params_dict)
        return None


class WebResourceManager:
    async_session = async_session_maker

    def __init__(self):
        self.url: str = None
        self.data: dict = None

    async def prepare_resource_data(self, url: str) -> WebResourceCreateDBSchema:
        self.url = url
        self.data: dict = await WebResourceParserFetcher(url=self.url).parse_detail_url()
        return WebResourceCreateDBSchema(**self.data)

    async def prepare_resource_from_file(self, urls_files: UploadFile):
        tasks: list = []
        task = asyncio.create_task(self.run_scheduler_save(urls_files=urls_files))
        tasks.append(task)
        result = await asyncio.gather(*tasks)
        return result[0]

    async def create(self) -> WebResourceResponseSchema:
        async with self.async_session() as session:
            response = await WebResourceDAO.create(obj_in=self.data, session=session)
            return WebResourceResponseSchema(**await response.a_to_dict(obj=response))

    async def create_schedule(self):
        async with self.async_session() as session:
            response = await WebResourceDAO.create(obj_in=self.data, session=session)
            return WebResourceResponseSchema(**await response.a_to_dict(obj=response))

    async def run_scheduler_save(self, urls_files: UploadFile) -> dict:
        all_resource: int = 0
        fail_append: int = 0

        if urls_files.content_type != "application/zip":
            raise HTTPException(status_code=404, detail="File not zip format")

        csv_file, zip_archive_files = await FileManager.read_zip_files(zip_file=urls_files)

        if csv_file:
            list_resource: list = await FileManager.read_csv_from_zip(
                csv_filename=csv_file,
                zip_files=zip_archive_files
            )
            all_resource = len(list_resource)
            for url in list_resource:
                try:
                    await self.prepare_resource_data(url=url)
                    await self.create_schedule()
                except Exception:
                    fail_append += 1
        return {
            "all_resource": all_resource,
            "success_append": all_resource - fail_append,
            "fail_append": fail_append
        }

    @classmethod
    async def get_all(cls):
        async with cls.async_session() as session:
            data = await WebResourceDAO.get_all(session=session)
            return data

    @classmethod
    async def update_by_obj(cls, obj_to_upd: WebResourceUpdateDBSchema):
        obj_id = obj_to_upd.id
        obj_to_upd_in: dict[str, Any] = obj_to_upd.model_dump(exclude={"id", "host"},)
        async with cls.async_session() as session:
            await WebResourceDAO.update(session, WebResourceDAO.model.id == obj_id, obj_in=obj_to_upd_in)

    @classmethod
    async def get_by_uuid(cls, obj_id: str):
        async with cls.async_session() as session:
            obj = await WebResourceDAO.find_one_or_none(session, WebResourceDAO.model.id == obj_id)
            if obj:
                return obj
            logger.exception(f"Object with uuid {obj_id} not found")
            raise HTTPException(status_code=404, detail=f"Object with uuid {obj_id} not found")