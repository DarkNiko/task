from typing import Any, Generic, TypeVar, Sequence

from fastapi import HTTPException
from pydantic import BaseModel
from sqlalchemy import delete, insert, select, update, Row, RowMapping
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from src.db.base import AbsBase
from src.utils.logger_manageer import logger

ModelType = TypeVar("ModelType", bound=AbsBase)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class BaseDAO(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    model = None

    @classmethod
    async def find_one_or_none(
        cls, session: AsyncSession, *filter, **filter_by
    ) -> ModelType | None:
        filter_by["is_deleted"] = False
        stmt = select(cls.model).filter(*filter).filter_by(**filter_by)
        result = await session.execute(stmt)
        logger.info(f"get request to table: {cls.model.__name__}")
        return result.scalars().one_or_none()

    @classmethod
    async def get_all(
        cls,
        session: AsyncSession,
        *filter,
        offset: int = 0,
        limit: int = 100,
        **filter_by,
    ) -> Sequence[Row[Any] | RowMapping | Any]:
        filter_by["is_deleted"] = False

        stmt = (
            select(cls.model)
            .filter(*filter)
            .filter_by(**filter_by)
            .offset(offset)
            .limit(limit)
        )
        result = await session.execute(stmt)
        logger.info(f"GET request to table: {cls.model.__name__}")
        return result.scalars().all()

    @classmethod
    async def create(
        cls, session: AsyncSession, obj_in: CreateSchemaType | dict[str, Any]
    ) -> ModelType | None:
        if isinstance(obj_in, dict):
            create_data = obj_in
        else:
            create_data = obj_in.model_dump(exclude_unset=True)
        try:
            stmt = insert(cls.model).values(**create_data).returning(cls.model)
            result = await session.execute(stmt)
            await session.commit()
            logger.info(f"Success CREATE obj in {cls.model.__name__}")
            return result.scalars().first()
        except (SQLAlchemyError, Exception) as e:
            if isinstance(e, SQLAlchemyError):
                msg = f"Database Exc: Cannot insert data into table: {obj_in}"
                logger.exception(f"ERROR from {cls.model}: {msg}")
            elif isinstance(e, Exception):
                msg = f"Unknown Exc: Cannot insert data into table {obj_in}"
                logger.exception(f"ERROR from {cls.model}: {msg}")
            raise HTTPException(status_code=400, detail="Probably with database")

    @classmethod
    async def delete(cls, session: AsyncSession, *filter, **filter_by) -> None:
        stmt = delete(cls.model).filter(*filter).filter_by(**filter_by)
        await session.execute(stmt)
        await session.commit()
        logger.info(f"DELETE request to table: {cls.model.__name__}")

    @classmethod
    async def update(
        cls,
        session: AsyncSession,
        *where,
        obj_in: UpdateSchemaType | dict[str, Any],
    ) -> ModelType | None:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.model_dump(exclude_unset=True)
        stmt = (
            update(cls.model)
            .where(*where)
            .values(**update_data)
            .returning(cls.model)
        )
        result = await session.execute(stmt)
        await session.commit()
        logger.info(f"UPDATE request to table: {cls.model.__name__}")
        return result.scalars().one()
