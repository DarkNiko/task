import os
from typing import Optional

from dotenv import load_dotenv
from pydantic import ConfigDict
from pydantic.fields import Field
from pydantic_settings import BaseSettings

load_dotenv()


class KafkaConnection(BaseSettings):
    bootstrap_servers: str = Field(alias="bootstrap.servers")
    group_id: Optional[str] = Field(alias="group.id", default=None)

    max_poll_interval_ms: int = Field(alias="max.poll.interval.ms")
    session_timeout_ms: int = Field(alias="session.timeout.ms")

    auto_offset_reset: str = Field(alias="auto.offset.reset", default="earliest")

    model_config = ConfigDict(
        frozen=False,
        populate_by_name=True,
    )


connect_settings = KafkaConnection(
    bootstrap_servers=os.getenv("KAFKA_BOOTSTRAP_SERVERS"),
    max_poll_interval_ms=os.getenv("KAFKA_STREAM_MAX_POLL_INTERVAL_MS"),
    session_timeout_ms=os.getenv("KAFKA_STREAM_SESSION_TIMEOUTS_MS")
)
