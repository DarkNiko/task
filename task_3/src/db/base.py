import uuid
from datetime import datetime
from typing import Type, TypeVar

from pydantic import BaseModel
from sqlalchemy import UUID, BigInteger, DateTime, MetaData, func
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column

from src.config import DB_NAMING_CONVENTION, UserIDType

T = TypeVar("T")
metadata = MetaData(naming_convention=DB_NAMING_CONVENTION)


class AbsBase(DeclarativeBase):
    __abstract__ = True
    metadata = metadata

    def to_dict(self) -> dict:
        result: dict = {
            key: value
            for key, value in self.__dict__.items()
            if not key.startswith("_")
        }
        return result

    def to_schema(self, schema: Type[T]) -> T:
        return schema(**self.to_dict())

    @classmethod
    def from_dict(cls, data: dict):
        valid_data: dict = {
            key: value for key, value in data.items() if key in cls.__dict__
        }
        return cls(**valid_data)

    @classmethod
    def from_schema(cls, data: BaseModel):
        return cls.from_dict(data.model_dump())

    def __repr__(self) -> str:
        return str(self.to_dict())


class BaseDB(AbsBase):
    __abstract__ = True

    id: Mapped[int] = mapped_column(
        BigInteger,
        autoincrement=True,
        nullable=False,
        unique=True,
        index=True,
        primary_key=True,
        comment="Identifier",
    )
    created_at: Mapped[datetime] = mapped_column(
        DateTime,
        default=func.now(),
        nullable=False,
        comment="Creation timestamp",
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime,
        default=func.now(),
        onupdate=func.now(),
        nullable=False,
        comment="Last update timestamp",
    )


class BaseUserDB(AbsBase):
    __abstract__ = True

    id: Mapped[UserIDType] = mapped_column(
        UUID,
        primary_key=True,
        index=True,
        default=uuid.uuid4,
        comment="Identifier",
    )
    created_at: Mapped[datetime] = mapped_column(
        DateTime,
        default=func.now(),
        nullable=False,
        comment="Creation timestamp",
    )
    updated_at: Mapped[datetime] = mapped_column(
        DateTime,
        default=func.now(),
        onupdate=func.now(),
        nullable=False,
        comment="Last update timestamp",
    )
