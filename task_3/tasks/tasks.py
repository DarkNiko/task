import asyncio

import aiohttp
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from pytz import utc

from src.apps.web_resource_monitoring.schemas import WebResourceUpdateDBSchema
from src.apps.web_resource_monitoring.service import WebResourceManager
from src.utils.logger_manageer import logger

scheduler = AsyncIOScheduler(timezone=utc)


class CheckResourceStatus:
    ignore_result = True

    @classmethod
    async def check_from_resource(cls) -> None:
        url_data = await WebResourceManager.get_all()
        if url_data:
            await cls._net_runner_aiohttp(urls=url_data)
        else:
            logger.info("No Data for checking")

    @classmethod
    async def _fetch_url(cls, session, url_obj) -> int:
        try:
            async with session.get(url_obj.url) as response:
                if response.status <= 404:
                    logger.info(f"RESPONSE SCHEDULER CHECKER: {response.status}")
                    return response.status
                url_obj.status_code = response.status
                url_obj.is_deleted = True
                await WebResourceManager.update_by_obj(obj_to_upd=WebResourceUpdateDBSchema(**url_obj.__dict__))
                logger.warn(f"Resource {url_obj.url} is have bad request {response.status}")
                return response.status
        except Exception as err:
            logger.exception(f"Resource {url_obj.url} is have probably: {err}")
            url_obj.is_deleted = True
            await WebResourceManager.update_by_obj(obj_to_upd=WebResourceUpdateDBSchema(**url_obj.__dict__))

    @classmethod
    async def _net_runner_aiohttp(cls, urls: list):
        tasks: list = []
        async with aiohttp.ClientSession() as session:
            for url_obj in urls:
                task = asyncio.create_task(cls._fetch_url(session, url_obj))
                tasks.append(task)
            await asyncio.gather(*tasks)
